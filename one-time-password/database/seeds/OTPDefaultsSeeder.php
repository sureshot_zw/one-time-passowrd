<?php

use Illuminate\Database\Seeder;
use App\OTPSettings;

class OTPDefaultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(OTPSettings::all()->isNotEmpty()) {
            throw new Exception ('OTP settings have already been seeded');
        }

        OTPSettings::create([
            'setting' => 'length', 
            'value' => 6,
        ]);

        OTPSettings::create([
            'setting' => 'allowed_per_hour', 
            'value' => 3,
        ]);

        OTPSettings::create([
            'setting' => 'expiration', 
            'value' => 30, //seconds
        ]);

        OTPSettings::create([
            'setting' => 'resend_grace_period', 
            'value' => 300, //seconds
        ]);

        OTPSettings::create([
            'setting' => 'resend_max', 
            'value' => 3,
        ]);

        OTPSettings::create([
            'setting' => 'allowed_attempts', 
            'value' => 3,
        ]);

        
    }
}
