<?php

use Illuminate\Database\Seeder;
use App\User;
use Hash;
use Exception;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::where('name', 'My Test')->get()->isNotEmpty()) {
            throw new Exception('Test user already exists');
        }

        User::create([
            'name' => 'My Test', 
            'email' => 'no-reply@entrostat.xyz', 
            'password' => Hash::make(Str::random(10)),
        ]);
    }
}
