<?php

return [
    'defaults' => [
        'length' => 6, 
        'allowed_per_hour' => 3, 
        'expiration' => 30, 
        'resend_grace_period' => 300, 
        'resend_max' => 3,
        'allowed_attempts' => 3,
    ],
    'mail' => [
        'from' => [
            'address' => env('MAIL_FROM_ADDRESS', 'no-reply@entrostat.xyz'), 
            'name' => env('MAIL_FROM_NAME', 'One Time Password')
        ],
    ]
];