<h1>System Access OTP</h1>

<p>Hello {{ $email }},</p>

<p>You requested an OTP from the system. Your OTP is {{ $otp }}.</p>
<p>It was generated on {{ $generated }} and is valid for {{ $expiration }} seconds.

<p>Regards,</p>
</p>The Support Team</p>