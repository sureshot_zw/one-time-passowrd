System Access OTP

Hello {{ $email }},

You requested an OTP from the system. Your OTP is {{ $otp }}.
It was generated on {{ $generated }} and is valid for {{ $expiration }} seconds.

Regards,
The Support Team