<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class OtpValidationRequest extends FormRequest
{
    use SanitizesInput;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  Validation rules to be applied to the input.
     *
     *  @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email'],
            'otp' => ['required', 'string', 'exists:otps,token'],
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     *  @return array
     */
    public function filters()
    {
        return [
            'email' => 'trim|escape', 
            'otp' => 'trim|escape|digit', 
        ];
    }

    /**
     * Return email address
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Return otp
     * 
     * @return string
     */
    public function getOtp()
    {
        return $this->otp;
    }
}
