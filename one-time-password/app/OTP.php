<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\OTPSettings;

class OTP extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'token', 
        'validity',
        'expired',
        'generated_count',
        'attempted_count',
        'resend_count',
        'created_at',
    ];

    public $table = 'otps';

    /**
     * OTP is expired
     * 
     * @return bool
     */
    public function isExpired() :bool
    {
        if ($this->expired) return true;
        
        $generatedTime = $this->expiredAt();

        if ($generatedTime->greaterThan(Carbon::now()->toDateTimeString())) {
            return false;
        }

        $this->expired = true;
        $this->created_at = Carbon::now()->toDateTimeString();
        $this->save();

        return true;
    }

    /**
     * Time OTP expires at
     * 
     * @return object
     */
    public function expiredAt() :object
    {
        return $this->created_at->addSeconds($this->validity);
    }

    /**
     * Can be resent based on resend grace period
     * 
     * @return bool
     */
    public function canBeResent() : bool
    {
        if($this->expired) return false;
        
        $otpSetting = OTPSettings::where('setting', 'resend_grace_period')->first();

        if(empty($otpSetting)) {
            $resendGracePeriod = config('otp-config.defaults.resend_grace_period');
        }else {
            $resendGracePeriod = $otpSetting->value;
        }

        $resendTime = $this->created_at->addSeconds($resendGracePeriod);

        return $resendTime->greaterThan(Carbon::now()->toDateTimeString());
    }
}
