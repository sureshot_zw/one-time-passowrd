<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Services\OTPService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias(OTPService::class, 'otp-service');
        $this->mergeConfigFrom(__DIR__ . '/../../config/otp.php', 'otp-config');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
