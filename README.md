# One Time Password Application

This is a One Time Password application that allows for generation of OTP's with validation through a basic UI.
The OTP is sent through an email as well as being displayed on screen on generation. 

## STACK
* HTML
* CSS (Boostrap)
* VUEJS
* MYSQL

## HOW TO GET IT RUNNING
To get the application running after cloning do the following in the root level of the project folder
* `cp .env.example .env` - you will need to populate the new file with configuration values. 
* `composer install` - to install all dependencies.
* `php artisan migrate` - to create database tables.
* `php artisan db:seed` - seeds the login user as well as OTP default settings.
* `npm install && npm run dev` - compile dependencies for FE (Vuejs)

After this a simple run `php artisan serve` will do the trick a link of where to find the application will be provided in the terminal), you can also specify host and port if you are working on a setup with port forwarding e.g.
`php artisan serve --host=0.0.0.0 --port=8080`.

**This application was developed in a linux environment.**

The user required to login is created through database seeding with a random password. 
You will need to do a password reset to set your own password on user with email `no-reply@entrostat.xyz` and on reset you will be able to login with your new password.

## Some things I would do to expand the project
* Add unit tests to validate OTP operations.
* Coupled with unit testing, I believe intergrating DUSK for FE testing would be really neat and useful [Laravel Dusk](https://laravel.com/docs/7.x/dusk).
